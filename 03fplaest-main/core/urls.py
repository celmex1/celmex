from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from django.shortcuts import render
from core import views

app_name = "core"

urlpatterns = [
        path('', views.Index.as_view(), name="index"),

        path('accesorio/', views.accesorios.as_view(), name="accesorios"),
        path('recargas/', views.recargas.as_view(), name="recargas"),
        path('planes/', views.planes.as_view(), name="planes"),
        path('producto/', views.Producto.as_view(), name="product"),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)