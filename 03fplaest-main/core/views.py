from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from django.conf import settings
from home.models import *
# Create your views here.


class Index(generic.View):
    template_name = "core/celmex2.html"
    context = {}

    def get(self, request):
        self.context = {
            "products": Product.objects.all()
        }
        return render(request, self.template_name, self.context)


class Producto(generic.View):
    template_name = "home/product/product.html"
    context = {}
    def get(self, request):
        #Armasgay es la variable donde se guarda la consulta de base de datos
        self.context = {
            "Armasgay": Product.objects.all()
        }
        return render(request, self.template_name, self.context)

class accesorios(generic.View):
    template_name = "core/UsuarioAccesorios.html"
    context = {}

    def get(self, request):
        self.context = {
            "accesorios": Accesorios.objects.all()
        }
        return render(request, self.template_name, self.context)

class recargas(generic.View):
    template_name = "core/UsuarioRecargas.html"
    context = {}

    def get(self, request):
        self.context = {
            "recargas": Recargas.objects.all()
        }
        return render(request, self.template_name, self.context)

class planes(generic.View):
    template_name = "core/UsuarioPlanes.html"
    context = {}

    def get(self, request):
        self.context = {
            "planes": Planes.objects.all()
        }
        return render(request, self.template_name, self.context)