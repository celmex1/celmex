from django.urls import path, include
from django.contrib import admin
from sesion import views

app_name = "sesion"

urlpatterns = [

    path('login/', views.LoginView.as_view(), name="login"),
    path('logout/', views.logoutView, name="logout"),
    path('signup/', views.SignUp.as_view(), name="signup"),

]
