# Generated by Django 4.2.5 on 2023-12-05 04:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sesion', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='user',
        ),
        migrations.DeleteModel(
            name='UserPrueba',
        ),
        migrations.DeleteModel(
            name='Profile',
        ),
    ]
