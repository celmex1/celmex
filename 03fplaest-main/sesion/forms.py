from django import forms


from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User







class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu username"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"escribe tu password"}))


# class SignUpForm(forms.ModelForm):
#     class Meta:
#         model = User
#         fields = [
#             "username",
#             "password"
#         ]
#         # widgets = {
#         #     "username": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Username"}),
#         #     "password": forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Escribe tu Password"}),
#         #     "password1": forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Confirma tu Password"}),
#         # }



class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=140, required=True, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu First Name"}))
    username = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Username"}))
    password1 = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Escribe tu Password"}))
    password2 = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Repite tu Password"}))
    last_name = forms.CharField(max_length=140, required=False, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Last Name"}))
    email = forms.EmailField(required=True, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Email"}))

    class Meta:
        model = User
        fields = [
            'username',
            'password1',
            'password2',
            'email',
            'first_name',
            'last_name',
        ]
        # widgets = {
        #     "username": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Username"}),
        #     "password1": forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Escribe tu Password"}),
        #     "password2": forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Repite tu Password"}),
        #     "email": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Email"}),
        #     "first_name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu First Name"}),
        #     "last_name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Last Name"}),
        # }
