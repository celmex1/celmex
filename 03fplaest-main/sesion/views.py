from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy, reverse

from .forms import *


class LoginView(generic.View):
    template_name = "sesion/login.html"
    context = {}
    user = ""
    password = ""
    form_class = LoginForm
    
    def get(self, request):
        self.form_class = LoginForm()
        self.context = {
            "form": self.form_class
        }
        return render(request, self.template_name, self.context)
    
    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("/")
        else:
            return redirect("/login/")


def logoutView(request):
    logout(request)
    return redirect('/')



class SignUp(generic.CreateView):
    template_name = "sesion/signup.html"
    form_class = SignUpForm
    success_url = reverse_lazy("home:index")

    def form_valid(self, form):
        form.save()
        username = form.cleaned_data.get('username')
        password1 = form.cleaned_data.get('password1')
        user = authenticate(username=username, password=password1)
        login(self.request, user)
        return redirect('/')
